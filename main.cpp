#include <complex>
#include <random>
#include <chrono>
#include <stdio.h>
#include <string>
#include <windows.h>
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <stdexcept>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

template<typename T>
std::complex<T> iteration(const std::complex<T>& z, const std::complex<T>& c){
    return z * z + c;
}

template<typename T>
bool iterate(const std::complex<T>& z, const int numIts, std::vector<std::complex<T>>& poss){
    std::complex<T> comp = z;
    poss.resize(numIts);
    for(int i = 0; i < numIts; i++){
        comp = iteration(comp, z);
        poss[i] = comp;
        
        T re = comp.real(), im = comp.imag();
        if(re*re + im*im > 4.0){
            return false;
        }
    }
    return true;
}

void loadVars(const char* path, std::unordered_map<std::string, double>& varmap){
    std::string line;
    std::ifstream file(path);
    while(std::getline(file, line)){

        std::string word;
        std::string val;
        size_t space;
        if((space = line.find(" ")) == std::string::npos){
            continue;
        }
        word = line.substr(0, space);
        val = line.substr(space+1);
        varmap.emplace(word, atof(val.c_str()));


        line.clear();
    }
    file.close();
}

double get(const std::unordered_map<std::string, double>& map, const std::string& key, double def){
    try{
        return map.at(key);
    }
    catch (std::out_of_range){
        return def;
    }
}

//int getXCoord(int x, int ori){
    //return x - ori;
//}
//int getYCoord(int y, int ori){
    //return ori - y;
//}

int getXPix(double x, int ori, float xScale){
    return x*xScale + ori;
}

int getYPix(double y, int ori, float yScale){
    //return (yScale * y - ori);
    return (yScale * y);
    //return ori - yScale*y;
    printf("%f %d %f\n", y, ori, yScale);
}


int main(int argc, char const *argv[]){


    std::unordered_map<std::string, double> varmap;
    std::unordered_map<std::string, std::string> stringmap;
    loadVars("vars.dat", varmap);
    
    const unsigned int samples = get(varmap, "samples", 1000000);
    const unsigned int percentage = samples / 100;
    const unsigned int its = get(varmap, "its", 1000);
    const unsigned int xSize = get(varmap, "xSize", 1600), ySize = get(varmap, "ySize", 900);
    const int xOri = xSize * get(varmap, "xOri", 0.75) , yOri = ySize * get(varmap, "yOri", 0.5) ;
    //float xScale = 600, yScale = 600; //Experimentally achieved
    const double scale = ySize * get(varmap, "scale", 0.1);
    std::vector<unsigned int> buddhacounter(xSize * ySize, 0);

    std::mt19937 genx(std::chrono::system_clock::now().time_since_epoch().count());
    std::mt19937 geny(std::chrono::system_clock::now().time_since_epoch().count() + 1);
    std::uniform_real_distribution<double> distrx(-0.5, 2);
    std::uniform_real_distribution<double> distry(-1.3, 1.3);

    for(unsigned int i = 0; i < samples; i++){
        std::complex<double> point(distrx(genx), distry(geny));

        {//I want to use simple vars for overview purposes
            //but don't wan to pollute the namspace
            //Test if this point is inside the 2 main features of Mandlebrot
            double r = point.real(), y = point.imag();
            double q = (r-1.0/4)*(r-1.0/4) + y*y;
            if(q*(q + (y - 1.0/4)) < 1.0/4 *y*y){
                i--;
                continue;
            }
            if((r+1)*(r+1) + y*y < 1.0/16){
                i--;
                continue;
            }
        }

        std::vector<std::complex<double>> points; 
        if(!iterate<double>(point, its, points)){
            for(std::complex<double>& point : points){
                double re = point.real();
                double im = point.imag();
                //printf("%d, %d\n", re, im);
                int x = getXPix(re, xOri, scale);
                int y = getYPix(im, yOri, scale);
                if(x > 0 && x < xSize && y > 0 && y < ySize ){
                    buddhacounter[y * xSize + x]++;
                }
            }
        }
        //For Logging
        if (!(i % percentage)){
            printf("%d\%: %f + %fi\n", i/percentage, point.real(), point.imag());
        }
    }

    ///Get the biggest value in the vector
    double max = 1;
    for(unsigned int i = 0; i < buddhacounter.size(); i++){
        if (buddhacounter[i] > max){
            max = buddhacounter[i];
        }
    }

    sf::Image img;
    img.create(xSize, ySize);
    //Normalize the content of the vector with the maximum value and copy these to the image
    for(unsigned int x = 0; x < xSize; x++){
        for(unsigned int y = 0; y < ySize; y++){
            unsigned char val = buddhacounter[y * xSize + x] / max * 255.0;
            img.setPixel(x, y, sf::Color{val, val, val});
        }
    }
    img.saveToFile(std::string{"Buddhabrot.png"});
    printf("%d\%\n", 100);
    Beep(600, 750); // 600 hertz, 750 milliseconds

    return 0;
}
